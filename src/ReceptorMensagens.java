import java.io.IOException;

/**
 * Classe respons�vel por atuar como uma thread em looping infinito, receptora de mensagens
 * de um agente e executar ou delegar a��es necess�rias sobre essa mensagem.
 */
public class ReceptorMensagens implements Runnable {

	private Agente agente;
	private Mensagem mensagem;

	public ReceptorMensagens(Agente agente) {
		this.agente = agente;
	}

	@Override
	public void run() {
		while (true) {
			try {
				mensagem = agente.receberMensagem();
				if(mensagem.getRemetente() != agente.getTipo()){
					System.out.println(agente.getTipo().getTipoAgente() + " " + agente.getSocket().getLocalPort() +": " 
						+ mensagem.getMsg() + " ----- " + mensagem.getEnderecoRemetente().getPorta() + " " 
						+ mensagem.getRemetente().getTipoAgente());
				}
				agente.tratarMensagem(mensagem);
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
	}
}
