import java.io.Serializable;

/**
 * Classe de Mensagem Base que ser� trocada entre os agentes, implementa a interface
 * Serializable para que seja poss�vel a transfer�ncia dos objetos entre sockets UDP.
 * Cada agente dever� possuir sua pr�pia mensagem extendendo desta. 
 */
public class Mensagem implements Serializable {

	private static final long serialVersionUID = 4296383380001025316L;
	
	private String mensagem;
	private TipoAgente remetente;
	private EnderecoAgente enderecoRemetente;
	
	public Mensagem (TipoAgente remetente, String mensagem){
		this.setRemetente(remetente);
		this.mensagem = mensagem;
	}

	public String getMsg() {
		return mensagem;
	}

	public void setMsg(String msg) {
		this.mensagem = msg;
	}

	public TipoAgente getRemetente() {
		return remetente;
	}

	public void setRemetente(TipoAgente remetente) {
		this.remetente = remetente;
	}

	public EnderecoAgente getEnderecoRemetente() {
		return enderecoRemetente;
	}

	public void setEnderecoRemetente(EnderecoAgente enderecoRemetente) {
		this.enderecoRemetente = enderecoRemetente;
	}
}
