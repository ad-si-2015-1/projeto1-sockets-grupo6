import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Classe responsavel por manter ou gerar informacoes uteis de rede pertinentes a toda a aplicacao
 */
public class RedeUtil {
	
	/**
	 * Metodo responsavel por gerar aleatoriamente um IP pertecente a sub-rede local
	 * 
	 * @return InetAddres gerado aleatoriamente
	 * @throws UnknownHostException
	 */
	public static InetAddress gerarIPLocalAleatorio() throws UnknownHostException {
		String ipString = Inet4Address.getLocalHost().getHostAddress();
		String[] partesDoIP = ipString.split(Pattern.quote("."));
		Integer novoHost = new Integer(new Random().nextInt(255));
		partesDoIP[3] = String.valueOf(novoHost);
		StringBuilder novoIP = new StringBuilder();
		for(String parte : partesDoIP){
			novoIP.append(parte);
			novoIP.append('.');
		}
		novoIP.delete(novoIP.length()-1, novoIP.length());
		InetAddress IP = InetAddress.getByName(novoIP.toString());
		return IP;
	}
	
	/**
	 * Metodo responsavel por gerar aleatoriamente uma Porta entre o range pre determinado
	 * @return Integer gerado aleatoriamente
	 */
	public static Integer gerarPortaAleatoria(){
		return new Random().nextInt(50008 - 50001 + 1) + 50001;
	}
}
