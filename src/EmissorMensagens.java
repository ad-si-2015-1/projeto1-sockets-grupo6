import java.io.IOException;
import java.net.InetAddress;

/**
 * Classe respons�vel por atuar como uma thread em looping infinito, emissora de mensagens
 * de um agente e executar ou delegar a��es necess�rias sobre essa mensagem.
 */
public class EmissorMensagens implements Runnable {
	
	private Agente agente;
	private static Integer PORTA_ARANHA = 50000;
	
	public EmissorMensagens(Agente agente) {
		this.agente = agente;
	}

	@Override
	public void run() {
		while(true){
			switch(agente.getTipo().getTipoAgente()){
				case "ARANHA":
					try {
						System.out.println("ARANHA: Procurando um agente...");
						agente.enviarMensagemComAtraso(agente.getMensagem(), 
								InetAddress.getByName("localhost"),
								RedeUtil.gerarPortaAleatoria());
			
						} catch (IOException | InterruptedException e) {
							e.printStackTrace();
						}
				break;
				
				case "ZUMBI":
					Zumbi zumbi = (Zumbi) agente;
						try {
							if(!zumbi.preso()){
							agente.enviarMensagemComAtraso(agente.getMensagem(), 
									InetAddress.getByName("localhost"), 
									PORTA_ARANHA);
							}else{
								Thread.sleep(agente.getTempo());
								System.out.println("ZUMBI " + agente.getSocket().getLocalPort() + ": estou preso!!");
							}
						} catch (IOException | InterruptedException e) {
							e.printStackTrace();
						}
				break;
				
				case "CACADOR":
					try {
						agente.enviarMensagemComAtraso(agente.getMensagem(), 
								InetAddress.getByName("localhost"), 
								PORTA_ARANHA);
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				break;
				
				case "CURADOR":
					try {
						agente.enviarMensagemComAtraso(agente.getMensagem(), 
								InetAddress.getByName("localhost"), 
								PORTA_ARANHA);
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				break;
				
				case "RESPONDEDOR":
					Respondedor respondedor = (Respondedor) agente;
					if(!respondedor.zumbi() && !respondedor.preso()){
						try {
							agente.enviarMensagemComAtraso(agente.getMensagem(), 
									InetAddress.getByName("localhost"), 
									PORTA_ARANHA);
						} catch (IOException | InterruptedException e) {
							e.printStackTrace();
						}
					}
					else if(respondedor.zumbi() && respondedor.preso()){
						try {
							Thread.sleep(agente.getTempo());
							agente.printMensagem("Urhg, Sou um Respondedor Zumbi preso!! ARGH");
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				break;
			}
		}
	}
}
