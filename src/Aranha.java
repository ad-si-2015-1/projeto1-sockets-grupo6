import java.io.IOException;
import java.net.DatagramSocket;
import java.util.LinkedList;

/**
 * Classe agente Aranha
 */
public class Aranha extends Agente{
	
	private LinkedList<EnderecoAgente> agentesClassificados;
	
	public Aranha(DatagramSocket socket, long tempo) {
		super(socket, tempo);
		super.setTipo(TipoAgente.ARANHA);
		super.setMensagem(new MensagemAranha(super.getTipo(), "Eu quero seu IP e sua Porta!!!!!!!"));
		this.agentesClassificados = new LinkedList<EnderecoAgente>();
	}


	@Override
	public void tratarMensagem(Mensagem mensagem) {
		MensagemAranha msgAranha = (MensagemAranha) mensagem;
		if(!msgAranha.pergunta()){
			EnderecoAgente endereco = new EnderecoAgente();
			endereco.setIp(msgAranha.getIpEncontrado());
			endereco.setPorta(msgAranha.getPortaEncontrada());
			agentesClassificados.addLast(endereco);
			System.out.println("ARANHA: Adicionando endereco na minha lista!!");
		}else{
			if(!agentesClassificados.isEmpty()){
				MensagemAranha aranhaResposta = new MensagemAranha(TipoAgente.ARANHA, "Pegue esse IP e essa porta e va ate la!!!");
				EnderecoAgente enderecoResposta = agentesClassificados.removeFirst();
				aranhaResposta.setIpEncontrado(enderecoResposta.getIp());
				aranhaResposta.setPortaEncontrada(enderecoResposta.getPorta());
				aranhaResposta.setPergunta(false);
				try {
					this.enviarMensagem(aranhaResposta,
										msgAranha.getEnderecoRemetente().getIp(), 
										msgAranha.getEnderecoRemetente().getPorta());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}else{
				System.out.println("ARANHA: Minha lista esta vazia, ainda nao encontrei ninguem!");
			}
		}
	}
	
	public LinkedList<EnderecoAgente> getAgentesClassificados() {
		return agentesClassificados;
	}
	
	public void setAgentesClassificados(LinkedList<EnderecoAgente> agentesClassificados) {
		this.agentesClassificados = agentesClassificados;
	}
}
