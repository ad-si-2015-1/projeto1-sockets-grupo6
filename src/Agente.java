import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Classe Abstrata Base para todos os agentes da aplica��o, cada agente dever� extende-la,
 * implementando suas funcionalidades espec�ficas.
 */
public abstract class Agente {
	private TipoAgente tipo;
	private DatagramSocket socket;
	private long tempo;
	private Mensagem mensagem;
	
	public Agente(DatagramSocket socket, long tempo){
		this.socket = socket;
		this.tempo = tempo;
	}
	
	public void enviarMensagem(Mensagem mensagem) throws IOException, InterruptedException{
		mensagem.setEnderecoRemetente(new EnderecoAgente(this.socket.getLocalAddress(), this.socket.getLocalPort()));
		byte[] sendData = new byte[1024];
		sendData = ConversorMensagens.mensagemParaBytes(mensagem);
		InetAddress IPAddress = InetAddress.getByName("localhost");
		socket.send(new DatagramPacket(sendData, sendData.length, IPAddress, 5000));
	}
	
	public void enviarMensagem(Mensagem mensagem, InetAddress IPAddress, Integer port) throws IOException, InterruptedException{
		mensagem.setEnderecoRemetente(new EnderecoAgente(this.socket.getLocalAddress(), this.socket.getLocalPort()));
		byte[] sendData = new byte[1024];
		sendData = ConversorMensagens.mensagemParaBytes(mensagem);
		IPAddress = InetAddress.getByName("localhost");
		socket.send(new DatagramPacket(sendData, sendData.length, IPAddress, port));
	}
	
	public void enviarMensagemComAtraso(Mensagem mensagem, InetAddress IPAddress, Integer port) throws IOException, InterruptedException{
		Thread.sleep(this.tempo);
		enviarMensagem(mensagem, IPAddress, port);
	}

	public Mensagem receberMensagem() throws IOException, ClassNotFoundException{
		DatagramPacket packet;
		byte[] receiveData = new byte[1024];
		packet = new DatagramPacket(receiveData, 1024);
		socket.receive(packet);
		Mensagem mensagem = (Mensagem) ConversorMensagens.mensagemDeBytes(packet.getData());
		return mensagem;
	}
	
	public void printMensagem(String mensagem){
		System.out.println(this.getTipo().getTipoAgente()+ " " + this.getSocket().getLocalPort() + ": " + mensagem);
	}
	
	public abstract void tratarMensagem(Mensagem mensagem);
	
	public TipoAgente getTipo() {
		return tipo;
	}

	public void setTipo(TipoAgente tipo) {
		this.tipo = tipo;
	}

	public Mensagem getMensagem() {
		return mensagem;
	}

	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}
	
	public DatagramSocket getSocket(){
		return this.socket;
	}

	public long getTempo() {
		return tempo;
	}
}
