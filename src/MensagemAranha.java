import java.net.InetAddress;

/**
 * Classe mensagem da Aranha 
 */
public class MensagemAranha extends Mensagem {
	private static final long serialVersionUID = -6674965232619539089L;
	private InetAddress ipEncontrado;
	private Integer portaEncontrada;
	private Boolean isPergunta;

	public MensagemAranha(TipoAgente remetente, String mensagem) {
		super(remetente, mensagem);
		this.isPergunta = true;
	}

	public InetAddress getIpEncontrado() {
		return ipEncontrado;
	}

	public void setIpEncontrado(InetAddress ipEncontrado) {
		this.ipEncontrado = ipEncontrado;
	}

	public Integer getPortaEncontrada() {
		return portaEncontrada;
	}

	public void setPortaEncontrada(Integer portaEncontrada) {
		this.portaEncontrada = portaEncontrada;
	}

	public boolean pergunta() {
		return isPergunta;
	}

	public void setPergunta(Boolean isPergunta) {
		this.isPergunta = isPergunta;
	}


}
