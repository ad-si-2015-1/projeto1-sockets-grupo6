import java.io.IOException;
import java.net.DatagramSocket;

/**
 * Classe agente Cacador
 */
public class Cacador extends Agente {

	public Cacador(DatagramSocket socket, long tempo) {
		super(socket, tempo);
		super.setTipo(TipoAgente.CACADOR);
		super.setMensagem(new MensagemAranha(super.getTipo(), "Quero um IP e uma porta para investigar!!!"));
	}

	@Override
	public void tratarMensagem(Mensagem mensagem) {
		switch (mensagem.getRemetente().getTipoAgente()){
		case "ARANHA":
			MensagemAranha msgAranha = (MensagemAranha) mensagem;
			if(!msgAranha.pergunta()){				
				try {
					this.enviarMensagem(new MensagemCacador(TipoAgente.CACADOR, "Voce esta PRESO!"),
							 msgAranha.getIpEncontrado(), 
							 msgAranha.getPortaEncontrada());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}else{				
				msgAranha.setIpEncontrado(this.getSocket().getInetAddress());
				msgAranha.setPortaEncontrada(this.getSocket().getLocalPort());
				msgAranha.setPergunta(false);
				msgAranha.setMsg("Enviando para voce meu IP e minha Porta!");
				msgAranha.setRemetente(TipoAgente.CACADOR);
				try {
					this.enviarMensagem(msgAranha, 
							msgAranha.getEnderecoRemetente().getIp(),
							msgAranha.getEnderecoRemetente().getPorta());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}				
		break;
		
		case "ZUMBI":
			try {
				this.enviarMensagem(new MensagemCacador(TipoAgente.CACADOR, "Voce esta PRESO!"),
						 mensagem.getEnderecoRemetente().getIp(), 
						 mensagem.getEnderecoRemetente().getPorta());
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		break;
		}
	}
}
