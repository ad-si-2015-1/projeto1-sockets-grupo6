/**
 * Enum de definição para os tipos de agentes disponíveis na aplicação.
 */
public enum TipoAgente {
	ARANHA ("ARANHA"),
	QUESTIONADOR ("QUESTIONADOR"),
	RESPONDEDOR ("RESPONDEDOR"),
	ZUMBI ("ZUMBI"),
	CACADOR ("CACADOR"),
	CURADOR ("CURADOR");
	
	private final String tipoAgente;
	
	private TipoAgente(String tipo){
		this.tipoAgente = tipo;
	}

	public String getTipoAgente() {
		return tipoAgente;
	}
}
