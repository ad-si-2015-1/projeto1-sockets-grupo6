import java.net.InetAddress;


public class MensagemZumbi extends Mensagem{

	private static final long serialVersionUID = 4287599371793108434L;
	private InetAddress ipZumbi;
	private Integer portaZumbi;
	
	
	public MensagemZumbi(TipoAgente remetente, String mensagem) {
		super(remetente, mensagem);
		// TODO Auto-generated constructor stub
	}
	
	public MensagemZumbi(TipoAgente remetente, String mensagem, InetAddress ip, Integer porta) {
		super(remetente, mensagem);
		this.ipZumbi = ip;
		this.portaZumbi = porta;
		
	}
	
	public InetAddress getIpZumbi() {
		return ipZumbi;
	}

	public void setIpZumbi(InetAddress ipZumbi) {
		this.ipZumbi = ipZumbi;
	}

	public Integer getPortaZumbi() {
		return portaZumbi;
	}

	public void setPortaZumbi(Integer portaZumbi) {
		this.portaZumbi = portaZumbi;
	}


}
