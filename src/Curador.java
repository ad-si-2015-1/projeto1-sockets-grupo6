import java.io.IOException;
import java.net.DatagramSocket;


public class Curador extends Agente {

	public Curador(DatagramSocket socket, long tempo) {
		super(socket, tempo);
		super.setTipo(TipoAgente.CURADOR);
		super.setMensagem(new MensagemAranha(super.getTipo(), "Quero um IP e uma porta para curar caso esteja infectado!!!"));
		// TODO Auto-generated constructor stub
	}

	@Override
	public void tratarMensagem(Mensagem mensagem) {
		switch (mensagem.getRemetente().getTipoAgente()){
		case "ARANHA":
			MensagemAranha msgAranha = (MensagemAranha) mensagem;
			if(!msgAranha.pergunta()){				
				try {
					this.enviarMensagem(new MensagemCurador(TipoAgente.CURADOR, "Beba a pocao da cura"),
							 msgAranha.getIpEncontrado(), 
							 msgAranha.getPortaEncontrada());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}else{				
				msgAranha.setIpEncontrado(this.getSocket().getInetAddress());
				msgAranha.setPortaEncontrada(this.getSocket().getLocalPort());
				msgAranha.setPergunta(false);
				msgAranha.setMsg("Enviando meu IP e Porta de curador!!");
				msgAranha.setRemetente(TipoAgente.CURADOR);
				try {
					this.enviarMensagem(msgAranha, 
							msgAranha.getEnderecoRemetente().getIp(),
							msgAranha.getEnderecoRemetente().getPorta());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}				
		break;
		
		case "ZUMBI":
			try {
				this.enviarMensagem(new MensagemCurador(TipoAgente.CURADOR, "Beba a pocao da cura"),
						 mensagem.getEnderecoRemetente().getIp(), 
						 mensagem.getEnderecoRemetente().getPorta());
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}					
		break;
		
		case "CACADOR":
			this.printMensagem("Sou um curador, estamos do mesmo lado, Cacador! ");
		break;
		
		
	}
		
	}

}
