/**
 * 
 * Classe mensagem do Curador
 *
 */
public class MensagemCurador extends Mensagem{

	private static final long serialVersionUID = 5459722451334298817L;

	public MensagemCurador(TipoAgente remetente, String mensagem) {
		super(remetente, mensagem);		
	}

}
