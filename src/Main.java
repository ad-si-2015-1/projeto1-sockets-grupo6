import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * Classe de demonstracao do funcionamento da comunicacao entre agentes, as mensagens serao 
 * maioritariamente formada por uma string semelhante a: 
 * 
 * 			ARANHA 5000: Quero um IP e uma porta para eu MORDEER!!! ----- 5002 ZUMBI
 * 
 * onde a primeira parte representa o agente que esta recebendo a mensagem e sua porta na maquina
 * local. Ja o fim da mensagem apos "-----" representa o remetente da mensagem e sua porta de 
 * localizacao na rede.
 */
public class Main {

	public static void main(String[] args) throws UnknownHostException, SocketException {
		
		if(args.length == 0){
	
		DatagramSocket socket = new DatagramSocket(50000);
		DatagramSocket socket2 = new DatagramSocket(50001);
		DatagramSocket socket3 = new DatagramSocket(50002);
		DatagramSocket socket4 = new DatagramSocket(50005);
		DatagramSocket socket5 = new DatagramSocket(50006);
		DatagramSocket socket6 = new DatagramSocket(50007);
		
		Agente aranha = new Aranha(socket, 2000);
		Agente zumbi = new Zumbi(socket2, 7000);
		Agente cacador = new Cacador(socket3, 8000);
		Agente questionador = new Questionador(socket4, 6000);
		Agente respondedor = new Respondedor(socket5, 5000);
		Agente curador = new Curador(socket6, 6000);
		
		new Thread(new EmissorMensagens(aranha)).start();
		new Thread(new ReceptorMensagens(aranha)).start();
		new Thread(new EmissorMensagens(zumbi)).start();
		new Thread(new ReceptorMensagens(zumbi)).start();
		new Thread(new EmissorMensagens(cacador)).start();
		new Thread(new ReceptorMensagens(cacador)).start();
		new Thread(new ReceptorMensagens(questionador)).start();
		new Thread(new EmissorMensagens(respondedor)).start();
		new Thread(new ReceptorMensagens(respondedor)).start();
		new Thread(new EmissorMensagens(curador)).start();
		new Thread(new ReceptorMensagens(curador)).start();
		}else{
			switch(args[0]){
				case "ARANHA":
					DatagramSocket socket = new DatagramSocket(50000);
					Agente aranha = new Aranha(socket, 2000);
					new Thread(new EmissorMensagens(aranha)).start();
					new Thread(new ReceptorMensagens(aranha)).start();
					System.out.println("ARANHA executando!!!");
					break;
				
				case "ZUMBI":
					DatagramSocket socket2 = new DatagramSocket(50001);
					Agente zumbi = new Zumbi(socket2, 7000);
					new Thread(new EmissorMensagens(zumbi)).start();
					new Thread(new ReceptorMensagens(zumbi)).start();
					System.out.println("ZUMBI executando!!!");
					break;
				
				case "CACADOR":
					DatagramSocket socket3 = new DatagramSocket(50002);
					Agente cacador = new Cacador(socket3, 8000);
					new Thread(new EmissorMensagens(cacador)).start();
					new Thread(new ReceptorMensagens(cacador)).start();
					System.out.println("CACADOR executando!!!");
					break;
					
				case "QUESTIONADOR":
					DatagramSocket socket4 = new DatagramSocket(50003);
					Agente questionador = new Questionador(socket4, 6000);
					new Thread(new ReceptorMensagens(questionador)).start();
					System.out.println("QUESTIONADOR executando!!!");
					break;
				
				case "RESPONDEDOR":
					DatagramSocket socket5 = new DatagramSocket(50006);
					Agente respondedor = new Respondedor(socket5, 5000);
					new Thread(new EmissorMensagens(respondedor)).start();
					new Thread(new ReceptorMensagens(respondedor)).start();
					System.out.println("RESPONDEDOR executando!!!");
					break;
					
				case "CURADOR":
					DatagramSocket socket6 = new DatagramSocket(50007);
					Agente curador = new Curador(socket6, 6000);
					new Thread(new EmissorMensagens(curador)).start();
					new Thread(new ReceptorMensagens(curador)).start();
					System.out.println("CURADOR executando!!!");
					break;
				
				default:
					System.out.println("ARGUMENTO INVALIDO");
					break;
			}
		}
		
	}

}
