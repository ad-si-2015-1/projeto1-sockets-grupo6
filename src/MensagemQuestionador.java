
/**
 * Classe mensagem do Questionador, responsavel por representar desafios matematicos, gera-los e avaliar as repostas
 * dos desafios que serao trocados entre o Questionador e Respondedor, contendo campos de operador e operandos alem do
 * campo de resposta para preenchimento do Respondedor.
 */
public class MensagemQuestionador extends Mensagem {

	private static final long serialVersionUID = 8781363442186846928L;
	
	private Integer campo1;
	private Integer campo2;
	private Character operador;
	private Integer resposta;
	private Boolean pergunta;
	
	public static final Character[] operadores = {'+', '-', '/', '*'};
	
	public MensagemQuestionador(TipoAgente remetente, String mensagem) {
		super(remetente, mensagem);
		this.pergunta = true;
	}
	
	public Integer getCampo1() {
		return campo1;
	}

	public void setCampo1(Integer campo1) {
		this.campo1 = campo1;
	}

	public Integer getCampo2() {
		return campo2;
	}

	public void setCampo2(Integer campo2) {
		this.campo2 = campo2;
	}

	public Character getOperador() {
		return operador;
	}

	public void setOperador(Character operador) {
		this.operador = operador;
	}

	public Integer getResposta() {
		return resposta;
	}

	public void setResposta(Integer resposta) {
		this.resposta = resposta;
	}
	
	public Boolean pergunta() {
		return pergunta;
	}

	public void setPergunta(Boolean pergunta) {
		this.pergunta = pergunta;
	}

}
