import java.io.IOException;
import java.net.DatagramSocket;


public class Respondedor extends Agente {
	
	private Boolean zumbi;
	private Boolean preso;

	public Respondedor(DatagramSocket socket, long tempo) {
		super(socket, tempo);
		super.setTipo(TipoAgente.RESPONDEDOR);
		super.setMensagem(new MensagemAranha(super.getTipo(), "Quero um IP e uma Porta para Questionar!!"));
		this.zumbi = false;
		this.preso = false;
	}

	@Override
	public void tratarMensagem(Mensagem mensagem) {
		switch (mensagem.getRemetente().getTipoAgente()){
		case "ARANHA":
			MensagemAranha msgAranha = (MensagemAranha) mensagem;
			if(!msgAranha.pergunta()){				
				try {
					this.enviarMensagem(new MensagemQuestionador(TipoAgente.RESPONDEDOR, "Me de um desafio!"),
							 msgAranha.getIpEncontrado(), 
							 msgAranha.getPortaEncontrada());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}else{				
				msgAranha.setIpEncontrado(this.getSocket().getInetAddress());
				msgAranha.setPortaEncontrada(this.getSocket().getLocalPort());
				msgAranha.setPergunta(false);
				msgAranha.setMsg("Enviando para voce meu IP e minha Porta!");
				msgAranha.setRemetente(TipoAgente.RESPONDEDOR);
				try {
					this.enviarMensagem(msgAranha, 
							msgAranha.getEnderecoRemetente().getIp(),
							msgAranha.getEnderecoRemetente().getPorta());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			}				
		break;
		
		
		case "QUESTIONADOR":
			if(this.zumbi()){
				if(!this.preso){
					MensagemZumbi msgZumbi = new MensagemZumbi(TipoAgente.ZUMBI, "MORDIDAAA DE UM ZUMBI RESPONDEDOR!!");
					try {
						this.enviarMensagem(msgZumbi, mensagem.getEnderecoRemetente().getIp(), 
								mensagem.getEnderecoRemetente().getPorta());
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				}else{
					this.printMensagem("Sou um respondedor zumbi preso arhg!!");
				}
			}else{
				MensagemQuestionador msgQuestionador = (MensagemQuestionador) mensagem;
				if(msgQuestionador.pergunta()){
					try {
						this.enviarMensagem(this.responderDesafio(msgQuestionador),
								msgQuestionador.getEnderecoRemetente().getIp(),
								msgQuestionador.getEnderecoRemetente().getPorta());
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		break;
		
		case "ZUMBI":
			if(!this.zumbi()){
				this.printMensagem("Urhhg, Virei ZUMBI arhg");
				this.setZumbi(true);
			}
		break;
			
		case "CACADOR":
			if(this.zumbi() && !this.preso){
				this.setPreso(true);
				this.printMensagem("Arhg, Sou um Zumbi Respondedor PRESOOO!!!");
			}
		break;
		
		case "CURADOR":
			if(this.zumbi()){
			   this.setPreso(false);
			   this.setZumbi(false);
			   this.printMensagem("Obrigado por me curar, sr. Curador!");
			}
		
		}	
	}
	
	public MensagemQuestionador responderDesafio(MensagemQuestionador desafio){
		switch(desafio.getOperador()){
			case '+':
				desafio.setResposta(desafio.getCampo1() + desafio.getCampo2());
			break;
			
			case '-':
				desafio.setResposta(desafio.getCampo1() - desafio.getCampo2());			
			break;
			
			case '*':
				desafio.setResposta(desafio.getCampo1() * desafio.getCampo2());
			break;
			
			case '/':
				desafio.setResposta(desafio.getCampo1() / desafio.getCampo2());
			break;
		}
		desafio.setPergunta(false);
		desafio.setMsg("Resposta: "+ desafio.getResposta().toString());
		desafio.setRemetente(TipoAgente.RESPONDEDOR);
		return desafio;
	}

	public Boolean zumbi() {
		return zumbi;
	}

	public void setZumbi(Boolean zumbi) {
		this.zumbi = zumbi;
	}

	public Boolean preso() {
		return preso;
	}

	public void setPreso(Boolean preso) {
		this.preso = preso;
	}

}
