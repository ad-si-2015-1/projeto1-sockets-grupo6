import java.io.Serializable;
import java.net.InetAddress;

/**
 * Classe para representar a localizacao de um Agente na rede
 */
public class EnderecoAgente implements Serializable {
	private static final long serialVersionUID = -509466311405236494L;
	private InetAddress ip;
	private Integer porta;
	
	public EnderecoAgente(){
	}
	
	public EnderecoAgente(InetAddress ip, Integer porta){
		this.ip=ip;
		this.porta=porta;
	}
	
	public InetAddress getIp() {
		return ip;
	}
	public void setIp(InetAddress ip) {
		this.ip = ip;
	}
	public Integer getPorta() {
		return porta;
	}
	public void setPorta(Integer porta) {
		this.porta = porta;
	}
}
