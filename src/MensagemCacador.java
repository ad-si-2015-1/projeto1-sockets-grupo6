import java.io.Serializable;

/**
 * Classe mensagem do Cacador
 */
public class MensagemCacador extends Mensagem implements Serializable {

	private static final long serialVersionUID = -6783583909435011578L;
	
	public MensagemCacador(TipoAgente remetente, String mensagem) {
		super(remetente, mensagem);
	}
}
