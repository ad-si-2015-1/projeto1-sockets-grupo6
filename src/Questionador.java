import java.io.IOException;
import java.net.DatagramSocket;
import java.util.Random;

/**
 * Classe agente Questionador
 */
public class Questionador extends Agente {
	
	private Boolean zumbi;
	private Boolean preso;

	public Questionador(DatagramSocket socket, long tempo) {
		super(socket, tempo);
		super.setTipo(TipoAgente.QUESTIONADOR);
		this.zumbi = false;
		this.preso = false;
	}

	@Override
	public void tratarMensagem(Mensagem mensagem) {
		switch (mensagem.getRemetente().getTipoAgente()){
			case "ARANHA":
				MensagemAranha msgAranha = (MensagemAranha) mensagem;
				if(msgAranha.pergunta()){							
					msgAranha.setIpEncontrado(this.getSocket().getInetAddress());
					msgAranha.setPortaEncontrada(this.getSocket().getLocalPort());
					msgAranha.setPergunta(false);
					msgAranha.setMsg("Enviando meu IP e porta para voce Aranha!");
					msgAranha.setRemetente(TipoAgente.QUESTIONADOR);
					try {
						this.enviarMensagem(msgAranha, 
								msgAranha.getEnderecoRemetente().getIp(),
								msgAranha.getEnderecoRemetente().getPorta());
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				}				
			break;
			
			case "RESPONDEDOR":
				if(this.zumbi()){
					if(!this.preso){
						MensagemZumbi msgZumbi = new MensagemZumbi(TipoAgente.ZUMBI, "MORDIDAAA DE UM ZUMBI QUESTIONADOR!!");
						try {
							this.enviarMensagem(msgZumbi, mensagem.getEnderecoRemetente().getIp(), 
									mensagem.getEnderecoRemetente().getPorta());
						} catch (IOException | InterruptedException e) {
							e.printStackTrace();
						}
					}else{
						this.printMensagem("Sou um questinador zumbi preso arhg!!");
					}
				}else{
					MensagemQuestionador msgQuestionador = (MensagemQuestionador) mensagem;
					if(msgQuestionador.pergunta()){
						try {
							this.enviarMensagem(this.gerarDesafio(),
									msgQuestionador.getEnderecoRemetente().getIp(),
									msgQuestionador.getEnderecoRemetente().getPorta());
						} catch (IOException | InterruptedException e) {
							e.printStackTrace();
						}
					}else{
						if(this.avaliarResposta(msgQuestionador)){
							MensagemQuestionador resposta = new MensagemQuestionador(TipoAgente.QUESTIONADOR, 
									"Resposta Certa!!!");
							resposta.setPergunta(false);
							try {
								this.enviarMensagem(resposta, 
										msgQuestionador.getEnderecoRemetente().getIp(), 
										msgQuestionador.getEnderecoRemetente().getPorta());
							} catch (IOException | InterruptedException e) {
								e.printStackTrace();
							}
						}else{
							System.out.println("RESPOSTA ERRADA!!");
						}
					}
				}
			break;
			
			case "ZUMBI":
				if(!this.zumbi()){
					this.printMensagem("Urhhg, Virei ZUMBI arhg");
					this.setZumbi(true);
				}
			break;
			
			case "CACADOR":
				if(this.zumbi() && !this.preso){
					this.setPreso(true);
					this.printMensagem("Arhg, Sou um Zumbi Questionador PRESOOO!!!");
				}
			break;
			
			case "CURADOR":
				if(this.zumbi()){
				   this.setPreso(false);
				   this.setZumbi(false);
				   this.printMensagem("Obrigado por me curar, sr. Curador!");
				}
		}
	}
	
	public MensagemQuestionador gerarDesafio(){
		MensagemQuestionador desafio = new MensagemQuestionador(TipoAgente.QUESTIONADOR, "Resolva este desafio!!!");
		desafio.setCampo1(gerarInteiroAleatorio(1, 1000));
		desafio.setCampo2(gerarInteiroAleatorio(1, 1000));
		desafio.setOperador(MensagemQuestionador.operadores[gerarInteiroAleatorio(0, 3)]);
		desafio.setPergunta(true);
		desafio.setMsg("Quanto e " + desafio.getCampo1().toString()
				+desafio.getOperador().toString()
				+desafio.getCampo2().toString()
				+" ?");
		return desafio;
	}
	
	public Boolean avaliarResposta(MensagemQuestionador desafio){
		Boolean resposta = false;
		if(desafio.getResposta() != null){
			switch(desafio.getOperador()){
				case '+':
					resposta = desafio.getCampo1() + desafio.getCampo2() == desafio.getResposta() ? true : false;
				break;
				
				case '-':
					resposta = desafio.getCampo1() - desafio.getCampo2() == desafio.getResposta() ? true : false;
				break;
				
				case '*':
					resposta = desafio.getCampo1() * desafio.getCampo2() == desafio.getResposta() ? true : false;
				break;
				
				case '/':
					resposta = desafio.getCampo1() / desafio.getCampo2() == desafio.getResposta() ? true : false;
				break;
			}
		}
		return resposta;
	}
	
	private static Integer gerarInteiroAleatorio(Integer inicio, Integer fim){
		if(fim == null){
			fim = Integer.MAX_VALUE;
		}
		return new Random().nextInt(fim - inicio + 1) + inicio;
	}

	public Boolean zumbi() {
		return zumbi;
	}

	public void setZumbi(Boolean zumbi) {
		this.zumbi = zumbi;
	}

	public Boolean preso() {
		return preso;
	}

	public void setPreso(Boolean preso) {
		this.preso = preso;
	}
}
