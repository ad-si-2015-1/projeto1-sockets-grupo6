
public class MensagemRespondedor extends Mensagem {

	private static final long serialVersionUID = 1689470892973961331L;

	public MensagemRespondedor(TipoAgente remetente, String mensagem) {
		super(remetente, mensagem);
	}

}
