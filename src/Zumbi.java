import java.io.IOException;
import java.net.DatagramSocket;


public class Zumbi extends Agente {
	
	private Boolean preso;

	public Zumbi(DatagramSocket socket, long tempo) {
		super(socket, tempo);
		super.setTipo(TipoAgente.ZUMBI);
		super.setMensagem(new MensagemAranha(super.getTipo(), "Quero um IP e uma porta para eu MORDEER!!!"));
		this.preso = false;
	}
	
	@Override
	public void tratarMensagem(Mensagem mensagem) {
		
		switch (mensagem.getRemetente().getTipoAgente()){
			case "ARANHA":
				MensagemAranha msgAranha = (MensagemAranha) mensagem;
				if(!msgAranha.pergunta()){				
					try {
						this.enviarMensagem(new MensagemZumbi(TipoAgente.ZUMBI, "Mordida!!!!"),
								 msgAranha.getIpEncontrado(), 
								 msgAranha.getPortaEncontrada());
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				}else{				
					msgAranha.setIpEncontrado(this.getSocket().getInetAddress());
					msgAranha.setPortaEncontrada(this.getSocket().getLocalPort());
					msgAranha.setPergunta(false);
					msgAranha.setMsg("URGHH!! Enviando meu IP e Porta!! URRGHH!!");
					msgAranha.setRemetente(TipoAgente.ZUMBI);
					try {
						this.enviarMensagem(msgAranha, 
								msgAranha.getEnderecoRemetente().getIp(),
								msgAranha.getEnderecoRemetente().getPorta());
					} catch (IOException | InterruptedException e) {
						e.printStackTrace();
					}
				}				
			break;
			
			case "ZUMBI":
				System.out.println("ZUMBI: Urrgh tem outro zumbi da porta "+
						mensagem.getEnderecoRemetente().getPorta() + " tentando me morder!!! ");
			break;
			
			case "CACADOR":
				this.setPreso(true);
			break;
			
			case "RESPONDEDOR":
				try {
					this.enviarMensagem(new MensagemZumbi(TipoAgente.ZUMBI, "Mordida!!!!"),
							 mensagem.getEnderecoRemetente().getIp(), 
							 mensagem.getEnderecoRemetente().getPorta());
				} catch (IOException | InterruptedException e) {
					e.printStackTrace();
				}
			break;
			
			case "CURADOR":
				this.printMensagem("Sempre fui Zumbi, nao preciso ser curado");
			break;
		}

	}

	public Boolean preso() {
		return preso;
	}

	public void setPreso(Boolean preso) {
		this.preso = preso;
	}

}
